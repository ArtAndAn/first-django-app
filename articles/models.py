from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils.crypto import get_random_string


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=200, unique=True)
    text = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(default='blog_default.png', blank=True)
    slug = models.SlugField(max_length=250, unique=True, blank=True, null=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return self.title

    def short_article(self):
        return {'title': self.title,
                'category': str(self.category).title(),
                'author': self.author,
                'created': self.created.strftime('%Y-%m-%d %H:%M'),
                'text': self.text,
                'image': self.image,
                'slug': self.slug}

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = get_random_string(50)
        super(Article, self).save(*args, **kwargs)
