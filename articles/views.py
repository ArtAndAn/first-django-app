from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

from articles.forms import NewArticleForm, NewCategoryForm
from articles.models import Category, Article


# Create your views here.
def articles_list(request):
    """Main page with all articles preview"""
    articles_data = [article.short_article() for article in Article.objects.all()]
    categories = [category.name.title() for category in Category.objects.all()]
    return render(request, 'articles/articles.html', context={'articles': articles_data,
                                                              'content': 'All articles ',
                                                              'title': 'All articles',
                                                              'categories': categories})


def category_articles_list(request, category_from_url):
    """Page with articles sorted by category (category_from_url)"""
    category = Category.objects.get(name=category_from_url.lower())
    articles_data = [article.short_article() for article in Article.objects.filter(category=category)]
    return render(request, 'articles/articles.html', context={'articles': articles_data,
                                                              'content': f'Articles for {category_from_url} category',
                                                              'title': category_from_url.title(),
                                                              'content_type': 'category'})


def user_articles_list(request, username):
    """Page with articles sorted by user (username)"""
    user = [None] if username == 'Anonymous_user' else User.objects.filter(username=username)
    articles_data = [article.short_article() for article in Article.objects.filter(author=user[0])] \
        if user and len(user) else []
    user = 'Anonymous user' if username == 'Anonymous_user' else username
    return render(request, 'articles/articles.html', context={'articles': articles_data,
                                                              'content': f'{user} articles:',
                                                              'title': f'{user} articles',
                                                              'content_type': 'user',
                                                              'user': user})


def full_article(request, slug):
    """Page with full article info"""
    article = Article.objects.get(slug=slug)
    return render(request, 'articles/full_article.html', context={'article': article})


@staff_member_required
def add_new_category(request):
    """Page for adding new category (available only for staff members)"""
    if request.method == 'POST':
        form = NewCategoryForm(request.POST)
        if form.is_valid():
            new_cat = form.save(commit=False)
            new_cat.name = new_cat.name.lower()
            if not Category.objects.filter(name=new_cat.name):
                new_cat.save()
                return redirect('home')
            form.add_error('name', 'Category with this name already exists.')
        return render(request, 'articles/add_form.html', context={'form': form, 'data_type': 'category'})
    form = NewCategoryForm()
    return render(request, 'articles/add_form.html', context={'form': form, 'data_type': 'category'})


@login_required(login_url='login')
def add_new_article(request):
    """Page for adding new article (available only for authorized users)"""
    if request.method == 'POST':
        form = NewArticleForm(request.POST)
        if form.is_valid():
            new_art = form.save(commit=False)
            new_art.author = request.user
            if not Article.objects.filter(title=new_art.title.lower()).exists():
                new_art.save()
                return redirect('home')
            form.add_error('title', 'Article with this title already exists.')
        return render(request, 'articles/add_form.html', context={'form': form,  'data_type': 'article'})
    form = NewArticleForm()
    return render(request, 'articles/add_form.html', context={'form': form, 'data_type': 'article'})


def add_data(request):
    """Page for deleting all saved articles and categories and adding new from function"""
    request.user = None if not request.user.is_authenticated else request.user

    Category.objects.all().delete()
    Article.objects.all().delete()

    Category.objects.bulk_create([
        Category(name='cars'),
        Category(name='sport'),
        Category(name='music'),
        Category(name='city'),
        Category(name='hobby'),
        Category(name='art'),
    ])

    Article(title='Kherson, Ukraine',
            text='Kherson, also spelled Cherson, city, southern Ukraine. It lies on the right (west) bank of the '
                 'lower Dnieper River about 15 miles (25 km) from the latter’s mouth. Kherson, named after the '
                 'ancient settlement of Chersonesus (west of what is now Sevastopol), was founded in 1778 as a '
                 'fortress to protect the newly acquired Black Sea frontage of Russia, and it became the first '
                 'Russian naval base and shipyard on the Black Sea. It was named a provincial capital in 1803. '
                 'The city grew steadily during the 19th century largely because of shipping and shipbuilding, and '
                 'it remained a major shipbuilding centre throughout the 20th century. Other industries have '
                 'included engineering, oil refining, and cotton-textile manufacturing. Kherson has a number of '
                 'training and research institutes. Pop. (2001) 328,360; (2005 est.) 319,278.',
            category=Category.objects.get(name='city'),
            author=request.user,
            image='Kherson.png').save()
    Article(title='New 2022 Audi Q6 e-tron electric SUV spied for the first time',
            text='Our spy photographers have caught the new Audi Q6 e-tron on camera ahead of its anticipated '
                 'arrival in 2022. The German brand’s latest electric SUV will make its debut next year, sharing '
                 'the same platform as the next-generation Porsche Macan. From these images of a heavily disguised '
                 'prototype we can see that the Audi Q6 e-tron’s design language will draw plenty of influence '
                 'from the e-tron GT four-door coupe. The SUV’s radiator grille and headlamps appear to be a '
                 'similar shape to the saloon’s, and there’s a similar full-width LED light-bar across the car’s'
                 ' tailgate. Inside, the car is likely to adopt the new 11.6-inch central touchscreen used in the'
                 ' Q4 e-tron, which is the largest display ever fitted to a production Audi. The interior design is'
                 ' likely to differ from the smaller MEB platformed Q4 e-tron, however, owing to the use of the new'
                 ' Premium Platform Electric (PPE) platform. The Q6 e-tron will be the first Audi based on the '
                 'Volkswagen Group’s new PPE architecture, which is being jointly developed by Audi and Porsche. '
                 'It’ll also be one of more than 20 electric cars the brand will launch by 2025, as the brand '
                 'targets for one third of its sales to come from EVs by the middle of the decade.',
            category=Category.objects.get(name='cars'),
            author=request.user,
            image='Audi_Q6.png').save()
    Article(title='Why Ukraine’s Small Paralympic Team Packs Such a Big Punch',
            text='The Ukrainians finished fifth in the overall medal standings in Tokyo with 98, just six fewer '
                 'than the United States, despite having a much smaller delegation.'
                 'TOKYO — In American sports terms, the Paralympians of Ukraine constitute a small-market marvel, '
                 'the Slavic equivalent of the Oakland Athletics.'
                 'At the Tokyo Paralympics, which ended Sunday, the Ukrainians finished fifth in the overall medal '
                 'standings with 98, just six fewer than the United States. Each of the top four countries — China,'
                 ' Britain, Russia and the United States — had more than 220 athletes in Tokyo, while Ukraine '
                 'brought 139.'
                 '“It’s a small country clearly punching above its weight,” said Craig Spence, the International '
                 'Paralympic Committee’s lead spokesman. '
                 'The success has not been matched by Ukraine’s Olympians, who were 16th in the Tokyo overall '
                 'medal standings last month. They won one gold medal, four fewer than Maksym Krypak, whose seven'
                 ' medals in swimming — five gold, plus one silver and one bronze — made him the most decorated '
                 'athlete of the Tokyo Paralympics. '
                 'Ukraine has been one of the top six countries in the medal count at nine consecutive Paralympic '
                 'Games, summer and winter, despite consistently being ranked among the poorest countries in Europe'
                 ' and cited by the United Nations as a difficult home for people with disabilities.',
            category=Category.objects.get(name='sport'),
            author=request.user,
            image='Paralympic.png').save()
    Article(title='Dance Against COVID-19: Eleven Interesting Summer Festivals in Ukraine',
            text='Will Ukraine’s best music and art festivals be postponed, or take place on schedule? We will keep'
                 ' track in this constantly-updated article'
                 'The coronavirus pandemic has changed the world of music and art festivals forever. Who knows when'
                 ' we will be ready to come together to enjoy fun and culture again? Ukrainian music and arts '
                 'festivals have been hit along with those everywhere else. Many organizers have already postponed'
                 ' their festivals, while others are still hoping for the best. Here are 11  awesome summer music '
                 'and culture festivals across Ukraine that are so far still planned as scheduled (Update: some of'
                 ' these have already been postponed).'
                 'ATLAS WEEKEND'
                 'Date: 7-12 July'
                 'Place: Kyiv, VDNG'
                 'Lineup: Twenty One Pilots, A$AP Rocky, Dimitri Vegas & Like Mike, Placebo, alt-J, '
                 'Two Door Cinema Club and more'
                 'Price: 120 EUR for five days, 50-65 EUR for one day'
                 'Atlas Weekend has been held since 2015. In its first iteration, it was a club-based festival '
                 'with a heavy focus on Ukrainian music. However, this focus has shifted towards a more traditional'
                 ' Western model of week long open-air festivals with camping and numerous side activities. Now, '
                 'it is one of the two biggest summer events on the Kyiv music scene.',
            category=Category.objects.get(name='music'),
            author=request.user,
            image='Dance_Against_COVID.png').save()
    Article(title='FC Dynamo Kyiv',
            text='Football Club Dynamo Kyiv (Ukrainian: Футбольний клуб «Динамо» Київ, pronounced [dɪˈnɑmo kɪjiu̯])'
                 ' is a Ukrainian professional football club based in Kyiv. Founded in 1927 as part of the Soviet '
                 'Dynamo Sports Society, the club plays in the Ukrainian Premier League, and has never been '
                 'relegated to a lower division. Their home is the 70,050 capacity Olimpiyskiy National Sports '
                 'Complex. Since 1936, Dynamo Kyiv has spent its entire history in the top league of Soviet and '
                 'later Ukrainian football. Its most successful periods are associated with Valeriy Lobanovskyi, '
                 'who coached the team during three stints, leading them to numerous domestic and European titles. '
                 'In 1961, the club became first-ever in the history of Soviet football that managed to overcome '
                 'the total hegemony of Moscow-based clubs in the Soviet Top League. The Spartak Moscow–Dynamo Kyiv'
                 'rivalry that began in the mid 1970s, is widely considered to have been one of the most exciting '
                 'football rivalries in the Soviet Union.[1] Since becoming the first Soviet football club to '
                 'participate in UEFA competition in 1965, Dynamo Kyiv has played in European competitions almost '
                 'every season. '
                 'Over its history, Dynamo Kyiv have won 16 Ukrainian top-fight league titles, 13 Soviet top-flight'
                 ' league titles, 11 Ukrainian national cup competitions, 9 Soviet national cup competitions, and '
                 'three continental titles (including two UEFA Cup Winners\' Cups). Its two European Cup Winners '
                 'Cups make it one of the only two Soviet clubs to have won a UEFA trophy, the other being Dinamo '
                 'Tbilisi. The Dynamo Kyiv first team became a base team for the Soviet Union national football '
                 'team in the 1970–1980s and the Ukraine national football team in the 1990–2000s. The two stars '
                 'on the club\'s crest each signify 10 top-flight seasons Dynamo Kyiv won. '
                 'The club was recognised as the Eastern European Club of the 20th Century by France-Presse,',
            category=Category.objects.get(name='sport'),
            author=request.user,
            image='FC_Dynamo_Kyiv.png').save()
    Article(title='Following your calling is life’s biggest runway',
            text='Stepan Krasilich was captivated by the sky at an early age. As a boy, when taking his cows out to '
                 'pasture, Stepan would climb a pear tree and cut a branch into the shape of an aeroplane, letting his '
                 'imagination fly:'
                 '“I would visualise it was a plane because I was so high up and with the cow '
                 'seemingly in the distance, I would fly in my imagination.” '
                 'As a young man he joined the army, and was finally asked the long-awaited question: whether he would '
                 'agree to fly. That’s how he got into the school of front-line aviation bombers.'
                 'Lukvytsia thought that all was lost when he began the helicopter section of his studies, since during'
                 ' this part they only flew a few times a year. However, a pilot by the name of Naidenko saved the '
                 'situation by taking Krasilich on board his MI-6 helicopter. From then on they began to fly together.'
                 'With time, the men’s life paths diverged. Naidenko went to study at the academy and Stepan Krasilich'
                 ' started working at Aeroflot. From the MI-6 he moved on to boarding the IL-28, the first Soviet jet'
                 ' bomber, and was transferred to two new location sites, the cities Kansk and Sverdlovsk in Russia. '
                 'His dream became a reality.'
                 'Stepan used every opportunity to be as close as possible to aviation. While working for Aeroflot, '
                 'Stepan Krasilich came to Vinnytsia, where he also worked as a flight engineer-tester at a local '
                 'aircraft factory for some time.'
                 'But as is always the case, the fulfillment of one desire always creates a new one ― something more '
                 'original, more ambitious. That’s when Stepan Krasilich began planning to create his own airfield so '
                 'that he could take up his favourite hobby independently of others.'
                 'The airfield. The beginning'
                 'During Soviet times, Stepan had to hide the idea of a future airfield, masking it as a food program '
                 'that was mandatory at the time. He made up a story that he wanted to start a beekeeping business in '
                 'the area and proposed that he would build hives.'
                 'In the beginning, Stepan Krasilich addressed his request to build an airfield to the commander of the'
                 ' flight squad, who assured him that nothing would come out of this idea.'
                 'Then, Stepan decided to turn to the head of the Lukvytsya collective farm for help. He visited the '
                 'head a few times on his motorcycle. And every time his request was denied. So, when Stepan was '
                 'visited by his old friend, former commander Valery Shcherbakov, they went together to yet another '
                 'meeting. But, most importantly, not on a motorcycle but in a helicopter.'
                 'I told him (the head ― ed.), that we want to be part of the food program, and he isn’t a stupid man, '
                 'he understood everything. And he told me: “You want a pond here, a bathhouse, a sauna, a cabin. Do I '
                 'understand correctly?”'
                 '“‘That’s right, Gnat Vasilyevich.’ He extends his hand and says: ‘A favour for a favour’. We shook '
                 'hands and he began to do the paperwork'
                 'And so we did all the paperwork, from the beginning and up to the executive committee. And then the '
                 'commander of the flight squad threw it all away. But when I got into the National Scout Organization '
                 'of Ukraine and offered this place, I already had some experience. And that’s how it happened.”',
            category=Category.objects.get(name='hobby'),
            author=request.user,
            image='Following_your_calling.png').save()
    Article(title='GogolTrain. The first legal art train',
            text='The great multi-disciplined festival of modern art StartUp GogolFest has taken place since 2018 in '
                 'Mariupol, where each spring hundreds of people from all over Ukraine go. For that purpose, '
                 'Bogdan Yaremchuk and Denys Uhorchuk created an art train, which takes the festival participants to '
                 'Mariupol, expanding the space for dialogue, interaction, and art. Organisers reached an agreement '
                 'with the Ministry of Infrastructure and with the help of governmental institutions decorated eight '
                 'train wagons with conceptual illustrations. GogolTrain is considered to be a scalable project of '
                 'cultural mobility — people may easily travel and attend cultural events in different regions of the '
                 'country. '
                 'GogolTrain is said to be the first art train in Europe. In the world, such an art project was held '
                 'for the first time in Canada in 1970. Back then Grateful Dead, Janis Joplin, The Band, '
                 'and other rock stars went on a tour from Toronto to Winnipeg to Calgary, arranging constant jam '
                 'sessions. In 2003, the documentary Festival Express was released about this journey. '
                 'The GogolTrain project was created by Bogdan Yaremchuk and Denys Uhorchuk, graduates of the Kyiv '
                 'Mohyla Academy and founders of the Cosmos Camp art group. When still a student, Denys bought up all '
                 'the tickets in a train wagon so he could take students to the Carpathians for a week. '
                 '— There were two of such trains — we called them rock ’n’ roll trains. It was absolutely an '
                 'interesting way of interaction for a big group of people. Initially you go, and there are a lot of '
                 'people around whom you don’t even know, but after two to three days of such a journey, '
                 'you treat each other as a family. It was such a funny method of ‘social glue’, tying people, '
                 'who are active, interesting by themselves, though for some reason have not met each other yet. '
                 'StartUp GogolFest took place for the first time in Mariupol in 2018. Back then, the team made an '
                 'agreement with Ukrzaliznytsia (Ukrainian Railways) about two additional train wagons for festival '
                 'participants from Kyiv. GogolTrain art director Andriy Yankovskyi recalls that he was going to '
                 'Mariupol as one of the ordinary passengers. '
                 '— By that time, the idea seemed already exciting, since for the majority of people it was the first '
                 'time they were going to visit Eastern Ukraine, except for in pictures or videos in terms of '
                 'preconception.',
            category=Category.objects.get(name='art'),
            author=request.user,
            image='Gogol_Train.png').save()
    Article(title='Buhai: a Bird and a Musical Instrument',
            text='There are a lot of folk musical instruments that have been forgotten over time, or their origin is '
                 'hard to establish. If no one produces or restores them, they can simply disappear. Among such rare '
                 'folk instruments is buhai, which was remembered in Ukraine only several years ago. The number of '
                 'craftsmen who make buhais is unknown, but Andrii Lopushynskyi is among them for sure. '
                 'Andrii Lopushynskyi moved to Daryivka village ten years ago from the city of Kherson. He lived in '
                 'the village together with his wife for eight years, and then they returned to the city. Now he '
                 'lives both in the city and the village. Andrii’s life is closely connected with history and museum '
                 'studies. The man was working in the Kherson Local History Museum for 15 years, and today he is a '
                 'researcher of “Khortytsia” Reserve with its branch “Kamyana Sich” in Tavria. He works there having '
                 'a dream to change the attitude of people to the museum as to the place for storing rarities only. '
                 'The first time Andrii got to know about a bird called buhai in Daryivka where these birds live on '
                 'the Inhulets river: '
                 '— One day I heard strange noises as if a pump was working. It was early in the morning, at 6 a.m. '
                 'approximately. I asked my wife: “Why did a farmer switch on his pump so early? It’s not a secret '
                 'that everyone pumps water from the river.” She replied: “It’s not a farmer, it’s a buhai.” And I '
                 'thought to myself: what is a “buhai”? She explained to me that it is a bird that lives in '
                 'bulrushes. Then I googled it, and she was right: there is such a bird. After a while, '
                 'I forgot about that incident. '
                 'Andrii has a well-recognized appearance: he has a scalp lock on his head and a dense moustache, '
                 'so you unwittingly associate him with a Cossack. And it is not a coincidence: he has been involved '
                 'in historical reconstruction for more than ten years. Together with his colleagues and friends, '
                 'Andrii makes clothes and things of Cossack everyday life. And seven years ago, his acquaintance, '
                 'who has a violin player friend, suggested him make a buhai. '
                 'Back then, Andrii knew just a bird with that name. His acquaintance with the same-named musical '
                 'instrument started with printouts and pictures. But Andrii already had had an experience of making '
                 'drums for historical clubs as well as body blanks so he managed to make his first buhai in 4 days. '
                 'He was not even sure that such musical instruments exist for real. Andrii says that there are no '
                 'secrets in the process: '
                 '— You look at it and see that it is made of wood, slats in particular, as well as iron and leather. '
                 'So you take slats, iron, and leather, and you make an item. If you have skillful hands, you do not '
                 'need any instructions. If one wants to learn how to make such an instrument, they will learn by '
                 'themselves. There are no secrets in the process. Just the materials are specific. For example, '
                 'it is easy to find wood – you can just buy it in the market. Finding good leather, though, '
                 'is more difficult: you need to take it from an old drum or mend it by yourself.',
            category=Category.objects.get(name='music'),
            author=request.user,
            image='Buhai.png').save()
    Article(title='Kayaks Are The Start',
            text='There are many wonderful and off-the-beaten-path destinations with incredible nature in Ukraine. In '
                 'early 2010, Andrii Kuzmenko and his band ‘Skryabin’ showcased one of them in their music video, '
                 '“Місця щасливих людей” (‘Places of happy people’ – ed.). It was shot at the Stanislav cliffs on the '
                 'Dnipro-Buh estuary in the Prychornomoria region. '
                 'Several years later two local lads, Oleh Marchuk and Andrii Holinko, moved back to their home '
                 'village, Oleksandrivka, and started promoting tourism in the region. They launched a tour agency '
                 'and now organize kayaking tours there. '
                 'Near Oleksandrivka village, the salt waters of the Black Sea meet the Southern Buh and Dnipro River '
                 'deltas creating the picturesque Dnipro-Buh estuary. On its shores, forty-metre, steep, '
                 'clay escarpments rise – Stanislav Cliffs – or simply “The Cliffs” or ”The Kherson Mountains”, '
                 'as the locals call them. In ancient times, the Scythians and Olbians lived here, centuries later '
                 'the Cossacks would set up their winter shelters here. Whoever the residents were, they fished in '
                 'nearby waters. However, due to poaching and overfishing, the river has ceased to be the primary '
                 'source of income for the villagers, who are abandoning their villages in increasing numbers to seek '
                 'jobs elsewhere. But the two friends have demonstrated that treating this land of salt lakes and '
                 'estuaries differently can unlock new potential. '
                 'Oleh and Andrii are childhood friends. They both grew up in Oleksandrivka and were passionate about '
                 'travelling and spending time in nature. As most of their peers, they settled down in cities as '
                 'adults, trying to realize their full potential. Oleh moved to Kryvyi Rih, where he both studied and '
                 'worked as a manager. Andrii received a Meteorology degree in Odesa. However, these two friends '
                 'could never get accustomed to the pace of life in a big city. '
                 'Oleh was the first to return. He explained that after the economic crisis of 2008, trouble was '
                 'starting to surface at work, so he left his job and moved back to Oleksandrivka. He recalls that '
                 'once back at the village he spent several years trying to discover what he wanted to do in life. At '
                 'the time his hobby was photography and video-making – so he decided to try to make a career of it; '
                 'he bought the equipment and started shooting. However, simply making money was not enough for Oleh. '
                 'What he wanted, he says, was to do something not just for himself, but for others as well. When '
                 'Andrii returned to the village, Oleh found in him a like-minded person who hadn’t gotten used to '
                 'the big city life either. '
                 '– Before this [moving back] my daughter was born, and I realised that it would be better to raise '
                 'the child somewhere unpolluted, with healthy, natural products and an eco-friendly environment. '
                 'Oleh says that he just couldn’t seem to stop thinking about how to improve, at least a little bit, '
                 'the living conditions in his home village. '
                 '– I’ve always wanted to do something for the villagers. I was like, ‘Yeah, I’m kinda OK, '
                 'I don’t need to go elsewhere to earn money. But still we aren’t doing anything for the village.’ We '
                 'continued to brainstorm… we wanted it to be something that was not expensive, but interesting.',
            category=Category.objects.get(name='sport'),
            author=request.user,
            image='Kayaks_Are_The_Start.png').save()
    Article(title='Motoball. A Big Ball Near the Fortress',
            text='Motoball is an exclusively European sport originating from France, which has gained enormous '
                 'popularity in Ukraine during Soviet Union times. Motoball tournaments and several motoball teams '
                 'have survived until today only on enthusiasm of players, coaches, and few spectators. In '
                 'Kamianets-Podilskyi motoball has already been existing for 52 years, and with a lack of local '
                 'teams’ success in other sports, the Podillia motoball club has become famous in Ukraine during this '
                 'time. There is a separate motoball stadium that you can find only in Kamianets-Podilskyi. Here, '
                 'motoball is a local highlight, still unknown to many guests. '
                 'Amotoball match is played on a football field with slightly different marks: the field doesn’t have '
                 'a central circle, and the goalpost area has a semi-circle shape. Asphalt and gravel are usually '
                 'used as a field cover. To improve motorcycle maneuverability, the asphalt is lightly sprinkled with '
                 'sand. The ball used for the sport is several times bigger than a football. Each team has five '
                 'players including a goalkeeper, and all players, with not exception, are on motorcycles. In '
                 'English, motoball is also called “motorcycle polo”. '
                 'There is almost no difference between a motoball bike and a regular cross motorcycle. The main '
                 'difference lies in the control levers. The motoball motorcycle is equipped with a duplicated '
                 'rear-brake pedal on either side of the bike. Since a motoball player uses one leg to control the '
                 'ball, the front wheel is armed with arches for moving the ball. The front of the motorcycle is also '
                 'equipped with “plows” that make it impossible for the ball to get under the motorcycle. In some '
                 'motorcycles of this type, gear-shift levers are connected directly to the handlebar to control the '
                 'motorcycle easier. '
                 'From a motorcyclist to a minibus driver'
                 'The oldest motoball player and the Kamianets-Podilskyi team captain, Volodymyr Danyliak, was once '
                 'in demand by many teams, but now he earns his living by driving a minibus. For six months he works '
                 'in Kyiv, and then, after the motoball season starts, he returns to Kamianets. Volodymyr’s love for '
                 'football on wheels has only grown with age. '
                 'Once in Sovetskiy Sport, a former Soviet newspaper that today is a Russian sports daily, there was '
                 'an article about “a big match under the walls of an old fortress”. It described the Kamianets '
                 'fortress and the motoball. The city was known only for the motoball team and the fortress. Many '
                 'people were coming to watch the games in 2008 and 2009 — back then the team was winning almost '
                 'every match. '
                 '— Currently there is a rapid team rejuvenation. Because other teams have only two older men, and, '
                 'well, I am the oldest in Ukraine, — said Volodymyr, 65. —I was training today — got such a buzz, '
                 'the real deal. My wife says, “Oh my god, you look so alive. Your eyes began to sparkle. So come on. '
                 'Keep up with the training.” As for now I work as a minibus driver in Kamianets. So, I got off the '
                 'route and had training, and now I’m going back to the route. For the last seven to eight years I '
                 'have tried every job.',
            category=Category.objects.get(name='sport'),
            author=request.user,
            image='Motoball.png').save()
    Article(title='Serhiivka: Sails over Reed',
            text='Serhiivka is known as a resort village in Bessarabia, not far from Odesa. However, this place bears '
                 'no resemblance to the neighboring Zatoka (resort destination): there are no hustle, night parties '
                 'or expensive boats. The steady pace of life that is predominant here has its own advantages. '
                 'Serhiivka is a climate and balneological resort, where one can indulge in swimming, drinking '
                 'mineral water and rubbing in therapeutic mud. However, basking in the sun is not for everyone. A '
                 'village at the bank of Budatskyi lyman (estuary) is a local sailing center. Here, on the small '
                 'boats under the sails, boys and girls regularly train in riding the whirlwind to see what they are '
                 'worth. They are the trainees of sailing school founded by Viacheslav and Oleksandr Smetanka. '
                 'Trainees’ boats are racing across the estuary to the Black Sea. Some children have just started to '
                 'dive under the boom without hitting their heads, while others are already Sailing Champions of '
                 'Ukraine, confidently holding the helm. '
                 'Viacheslav Smetanka offers us an overnight accommodation in one of the rooms of his recreation '
                 'center “Vodnyk”, while it is still an off-season in the sailing school. This resort is his own '
                 'man-made creation: he has built the summer house and paved the way to the estuary through the reed, '
                 'so that they could moor boats and set the sail. He drives an old “Zhyguli” car; he is also one of a '
                 'small number of people who talks to us in Ukrainian, although he admits that he is more used to '
                 'Russian. As he says, Russian used to be a language of Soviet military men, who would come to '
                 'Bessarabia to live to a ripe old age in their summer cottages amid the sun-drenched fruit and '
                 'vegetables. Viacheslav’s father also is a former military man. He met his wife-to-be in Kazakhstan, '
                 'where both of them were bringing the land under cultivation. Viacheslav points out being keen on '
                 'sailing since childhood: '
                 '— I was born here, in Serhiivka. When I was eight years old, to be more precise — 50 years ago, '
                 'I took up sailing. Some time later, I encouraged my younger brother Oleksandr to join me. '
                 'He took such a fancy to sport that he set an aim to build a base camp in his hometown, so that they '
                 'could host internationally recognized competitions. In 90s, Serhiivka hosted young athletes from '
                 'Great Britain. '
                 'Sailing Sister Town of Serhiivka'
                 '— In Ukraine, we call it a brother-town, but they say “sister town”, — so Viacheslav tells about '
                 'Rotherham, which is a hometown of yachtsmen who used to come to Ukraine to take part in a '
                 'competition. Due to its location in South Yorkshire along the both banks of the River Don, '
                 'Rotherham is considered to be the canter of sailing sport in England. The admirers of sails have '
                 'their trainings here on the bank of local water reservoir. Local yacht clubs are striving to '
                 'attract more visitors and that is why they regularly host competitions. In Rotherham, '
                 'one can satisfy all the needs of an avid sailsman. Be it a beginner or an experienced athlete, '
                 'here everyone can find a comfortable training spot. '
                 'Oleksandr and Viacheslav have once signed an agreement between Bilhorod-Dnistrovskyi lyceum, '
                 'Serhiivka school and British educational institutions to facilitate the exchange of experience '
                 'among children. Volunteer students from Great Britain taught English in Ukrainian schools and '
                 'Ukrainian children took part in sailing competition abroad. This is the project Viacheslav is proud '
                 'of: '
                 '— We have met Prime Minister. I even have a photo of Prime Minister of Great Britain, my brother, '
                 'our trainees and myself. '
                 'Thanks to Viacheslav’s “national” diplomacy, Serhiivka received ambulance equipment, intensive care '
                 'unit and medicine from Great Britain. They shared it just like that — asking nothing in return. '
                 'Collaboration with Great Britain lasted for 5 years. Later, as Viacheslav tells, former “red '
                 'typists” interfered. They did not do any good, so they had to stop the project. According to '
                 'Smetanka, it is time to revive it.',
            category=Category.objects.get(name='sport'),
            author=request.user,
            image='Sails_over_Reed.png').save()
    Article(title='Picasso and Malevich in Parkhomivka',
            text='The village of Parkhomivka in Slobozhanshchyna is famous for its art museum, which exhibits '
                 'world-renowned paintings, diagrams, sculptures, and archaeological and ethnographic works. The '
                 'museum collection appeared thanks to a village history teacher: the self-taught art expert, '
                 'Afanasii Lunov, who united his students around one common cause — art. '
                 'School museums in Ukraine started to appear between the 19th and 20th centuries, but they gained '
                 'widespread popularity only in the 1950s. At that time, school students participated mainly in the '
                 'search for valuable items, and in the organization of the exhibitions. Usually, exhibitions were '
                 'held in special rooms on the school premises, where the exhibits of regional history were displayed. '
                 'In the 1950s, Ukrainian art critic Borys Voznytskyi lived and taught in Vynnyky, a small town near '
                 'Lviv, where he created a room-museum. He exhibited collections that he obtained with his students '
                 'during expeditions to the nearest villages. The museum itself did not survive to the present day, '
                 'but lots of its exhibits are now displayed at the Historical Museum of the town of Vynnyky, '
                 'and in Olesko Castle. '
                 'The museum in Parkhomivka was also initially opened as a room-museum of regional history. Yet, '
                 'with the course of time, it scaled up due to the amount and value of its exhibits. Unlike the '
                 'museum in Vynnyky, this one is still working. For more than 30 years, it had been working on a '
                 'pro-bono basis. In 1986, it became a department of the Kharkiv Art Museum. '
                 'Since 2001, the museum in Parkhomivka has been managed by Olena Semenchenko, Afanasii (Panas) '
                 'Lunov’s student. Being a student of the 8th grade, she had an opportunity to try herself in the '
                 'role of a school museum director. Back then, high school students (students of the 8th–10th grades) '
                 'not only collected exhibits for the museum and gave tours after classes, but also could try '
                 'themselves in the roles of directors, conservators, etc. '
                 'Father of the museum'
                 'Afanasii Lunov was born in 1919 in the village of Shcheholok in Kursk governorate. As a teenager, '
                 'he and his family moved to Stalino (now Donetsk – ed.). Afanasii studied history at the Taras '
                 'Shevchenko National University of Kyiv. In October 1941, Lunov was captured by the Germans in '
                 'Nazi-occupied Kyiv. Till the end of the war, he had been forcibly kept at Nazi concentration camps. '
                 'After his return to the Ukrainian SSR in 1945, Lunov began teaching history at the secondary school '
                 'in Parkhomivka and obtained an external degree in history for the second time in Kharkiv. During '
                 'this period, he became interested in art, and spent most of his salary to collect artwork. In '
                 'Kharkiv, Lunov often visited Blahovishchenskyi flea market, where antique porcelain and valuable '
                 'paintings could be found in the postwar years. '
                 'Olena Semenchenko recollects that even back then her teacher tried to interest his students in art:'
                 '— He believed that the school curriculum was imperfect because it did not pay much attention to the '
                 'aesthetic education of children. Therefore, he offered his students to open a museum of regional '
                 'history in the school. The museum which would tell people about the history and culture of our '
                 'village, Parkhomivka.',
            category=Category.objects.get(name='art'),
            author=request.user,
            image='Picasso_and_Malevich.png').save()

    return redirect('home')
