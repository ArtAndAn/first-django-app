from . import views
from django.urls import path

urlpatterns = [
    path('', views.articles_list, name='home'),
    path('category/<str:category_from_url>', views.category_articles_list, name='category_articles'),
    path('user/<str:username>', views.user_articles_list, name='user_articles'),
    path('full/<str:slug>', views.full_article, name='full_article'),
    path('new_category', views.add_new_category, name='add_new_category'),
    path('new_article', views.add_new_article, name='add_new_article'),
    path('add_data', views.add_data, name='add_data'),
]
