from django.forms import ModelForm

from articles.models import Article, Category


class NewArticleForm(ModelForm):
    class Meta:
        model = Article
        exclude = ('author',)


class NewCategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ('name',)
