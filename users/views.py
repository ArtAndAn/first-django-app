from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.shortcuts import render, redirect

from users.models import RegisterForm


# Create your views here.
def sign_up(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
        else:
            return render(request, 'users/sign_up.html', context={'form': form})
    form = RegisterForm()
    return render(request, 'users/sign_up.html', context={'form': form})


def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            auth_login(request, form.get_user())
            return redirect('home')
        else:
            return render(request, 'users/login.html', context={'form': form})
    form = AuthenticationForm()
    return render(request, 'users/login.html', context={'form': form})


def logout(request):
    auth_logout(request)
    return redirect('home')
