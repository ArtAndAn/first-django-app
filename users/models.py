from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User


# Create your models here.
class RegisterForm(UserCreationForm):
    email = forms.EmailField(label="Email")
    firstname = forms.CharField(label="First name")
    lastname = forms.CharField(label="Last name")

    class Meta:
        model = User
        fields = ("username", "email", "firstname", "lastname")

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.first_name = self.cleaned_data["firstname"]
        user.last_name = self.cleaned_data["lastname"]
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user
