FROM alpine:3.13

# updating base image and python/pip installation
RUN apk update
RUN apk add python3 py3-pip

# configuration for successfull Pillow package installation
RUN apk add python3-dev py3-setuptools jpeg-dev zlib-dev gcc
RUN apk add --no-cache --virtual .build-deps build-base linux-headers

# setting working directory
WORKDIR /opt/django_blog

# requirements file copying and all dependencies installation
COPY requirements.txt ./
RUN pip3 install -r requirements.txt

# rest project files copying
COPY ./ ./

# setting 8000 port
EXPOSE 8000

# running manage.py file with command (runserver on 0.0.0.0:8000 by default)
ENTRYPOINT ["python3", "manage.py"]
CMD ["runserver", "0.0.0.0:8000"]
